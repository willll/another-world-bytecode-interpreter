#ifndef __AW_PARTS_
#define __AW_PARTS_

#include "endian.h"
#include "intern.hpp"

struct Game {
    // The game is divided into 10 parts.
    static const auto NUM_PARTS = 10;

    enum {
        PART_FIRST = 0x3E80,
        PART1 = 0x3E80,
        PART2 = 0x3E81,  // Introductino
        PART3 = 0x3E82,
        PART4 = 0x3E83,  // Wake up in the suspended jail
        PART5 = 0x3E84,
        PART6 = 0x3E85,  // BattleChar sequence
        PART7 = 0x3E86,
        PART8 = 0x3E87,
        PART9 = 0x3E88,
        PART10 = 0x3E89,
        PART_LAST = 0x3E89
    };
};

extern const uint16_t memListParts[Game::NUM_PARTS][4];

// For each part of the game, four resources are referenced.

struct MemlistPart {
    enum {
        PALETTE = 0,
        CODE = 1,
        POLY_CINEMATIC = 2,
        VIDEO2 = 3,
        NONE = 0x00    // WARNING : reused enum value !
    };
};
#endif
