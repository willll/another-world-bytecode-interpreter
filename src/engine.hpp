#ifndef __ENGINE_HPP__
#define __ENGINE_HPP__

/**
 * \file engine.hpp
 * \brief Game engine definition: the object which drives the game.
 * \author Montoir, Gregory
 * \author Sanglard, Fabien
 * \author Le Bars, Yoann
 * \version 2.1
 * \date 2021/04/26
 * \date 2021/04/27
 */

#include <string>

#include "intern.hpp"
#include "mixer.h"
#include "resource.hpp"
#include "sfxplayer.h"
#include "video.h"
#include "vm.h"
#include "sys.h"
#include "parts.h"

namespace AnotherWorld {
    /// \brief Definition of game engine.
    class Engine {
        public:
            /**
             * \brief Constructor.
             * \param paramSys System parameters.
             * \param dataDir Path to the directory where to find game assets.
             * \param saveDir Path to the directory where to save game state.
             */
            Engine (System *paramSys, const std::string &dataDir):
                        _dataDir (dataDir),
                        sys (paramSys),
                        mixer (sys),
                        res (&video, dataDir.c_str()),
                        video (&res, sys),
                        player (&mixer, &res, sys),
                        vm (&mixer, &res, &player, &video, sys) {
                sys->init("Out Of This World");

                video.init();

                res.allocMemBlock();

                res.readEntries();

                vm.init();

                mixer.init();

                player.init();

                /* At which part of the game to start. GAME_PART1 leads to
                   protection screen, while GAME_PART2 leads directly to game
                   introduction, bypassing the protection. To jump to further
                   part of the game, you also must set the game state,
                   otherwise it will crash. */
                uint16_t part = Game::PART1;
#ifdef BYPASS_PROTECTION
                part = Game::PART2;
#endif
                vm.initForPart(part);
            }

            /// \brief Destructor.
            ~Engine () {
                player.free();
                mixer.free();
                res.freeMemBlock();
                sys->destroy();
                sys = 0;
            }

            /// \brief Running the game.
            int run ();

        private:
            /// \brief Directory where to find game assets.
            std::string _dataDir;

            /// \brief System description.
            System *sys;

            /// \brief Object to mix game sound.
            Mixer mixer;

            /// \brief System ressources to be used into the game.
            Resource res;

            /// \brief Object for video window.
            Video video;

            /// \brief Object for audio Sfx.
            SfxPlayer player;

            /// \brief Virtual machine that runs game byte-code.
            VirtualMachine vm;
    };
}
#endif
