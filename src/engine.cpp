/**
 * \file engine.cpp
 * \brief Game engine implementation.
 * \author Montoir, Gregory
 * \author Sanglard, Fabien
 * \author Le Bars, Yoann
 * \version 2.1
 * \date 2021/04/26
 * \date 2021/04/27
 */

#include "engine.hpp"

int AnotherWorld::Engine::run () {
    while (!sys->input.quit) {
        vm.checkThreadRequests();

        vm.inp_updatePlayer();

        vm.hostFrame();
    }

    return 0;
}
