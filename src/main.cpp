/**
 * \file main.cpp
 * \brief A bytecode interpreter for Éric Chahi’s classical game Another World.
 * \author Montoir, Gregory
 * \author Sanglard, Fabien
 * \author Le Bars, Yoann
 * \version 2.1
 * \date 2021/04/24
 * \date 2021/04/25
 * \date 2021/04/26
 * \date 2021/04/27
 */

/**
 * \mainpage
 *
 * Raw is a reimplementation of the virtual machine for Another World. This
 * program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License  as published by the
 * Free Software Foundation; either version 2  of the License, or (at your
 * option) any later version. See file `LICENSE.txt` of go to:
 *
 * <https://choosealicense.com/licenses/gpl-2.0/>
 *
 * Command:
 *
 *     raw [Options]
 *
 * Supported options:
 *
 *     -h [ --help ]              Display this help message.
 *     -v [ --version ]           Display program version.
 *     -d [ --datapath ] arg (=.) Path to game data.
 *
 * Here are the various in game hotkeys:
 *
 *     - Arrow Keys: allow you to move Lester
 *     - Enter/Space: allow you run/shoot with your gun
 *     - C: allow to enter a code to jump at a specific level
 *     - P: pause the game
 *     - Alt X: exit the game
 *     - Ctrl F: toggle fast mode
 *     - TAB: change window scale factor
 *
 *
 * Overview
 * ========
 *
 * The game was originally made with 16 colors. Running on 320 × 200
 * (64,000 pixels.)
 *
 * A lot of details can be found regarding the game and engine architecture
 * at:
 *
 * <http://www.anotherworld.fr/anotherworld_uk/another_world.htm>
 *
 * Fabien Sanglard published an extensive code analysis, available at the
 * following URLs:
 *
 * <http://fabiensanglard.net/anotherWorld_code_review/index.php>
 *
 * <http://fabiensanglard.net/another_world_polygons/index.html>
 *
 * <http://fabiensanglard.net/another_world_polygons_PC_DOS/index.html>
 *
 * The chronology of the game implementation can retraced via the ordering of
 * the opcodes: The sound and music opcode are at the end: Music and sound was
 * done at the end.
 *
 *
 * Virtual Machine
 * ---------------
 *
 * Seems the threading model is collaborative multi-tasking (as opposed to
 * preemptive multitasking): A thread (called a Channel on Éric Chahi’s
 * website) will release the hand to the next one via the break opcode.
 *
 * It seems that even when a setvec is requested by a thread, we cannot set
 * the instruction pointer yet. The thread is allowed to keep on executing its
 * code for the remaining of the vm frame.
 *
 * A virtual machine frame has a variable duration. The unit of time is 20ms
 * and the frame can be set to live for 1 (20ms ; 50Hz) up to 5 (100ms ; 10Hz).
 *
 * 30 something opcode. The graphic opcode are more complex, not only the
 * declare the operation to perform they also define where to find the
 * vertices (segVideo1 or segVideo2).
 *
 * No stack available but a thread can save its pc (Program Counter) once: One
 * method call and return is possible.
 *
 *
 * Video
 * -----
 *
 * Double buffer architecture. AW optcodes even has a special instruction for
 * blitting from one frame buffer to another.
 *
 * Double buffering is implemented in software
 *
 * According to Éric Chahi’s webpage there are 4 framebuffer. Since on full
 * screen buffer is 320 x 200 / 2 = 32 kB that would mean the total size usage
 * is 128 kB?
 *
 * Sound
 * -----
 *
 * Mixing is done on software.
 *
 * Since the virtal machine and SDL are running simultaneously in two
 * different threads, any read or write to an elements of the sound channels
 * MUST be synchronized with a mutex.
 *
 *
 * Endianess
 * ---------
 *
 * Atari and Amiga used bigEndian CPUs. Data are hence stored within BANK in
 * big endian format. On an Intel or ARM CPU data will have to be transformed
 * when read.
 *
 *
 * Gregory Montoir’s codebase contained a looooot of cryptic hexa values.
 *
 * 0x100 (for 256 variables)
 *
 * 0x400 (for one kilobyte)
 *
 * 0x40 (for num threads)
 * 
 * 0x3F (num thread mask)
 *
 * Fabien Sanglard cleaned that up.
 *
 * Virtual Machine was named “logic” ?!?!
 *
 *
 * Questions & Answers
 * -------------------
 *
 * Q: How does the interpreter deals with the CPU speed? A pentium is way
 *    faster than a Motorola 68000 after all.
 *
 * A: See vm frame time: The vm frame duration is variable. The vm actually
 *    write for how long a video frame should be displayed in variable 0xFF.
 *    The value is the number of 20ms slice
 *
 * Q: Why is a palette 2048 bytes if there are only 16 colours? I would have
 *    expected 48 bytes...
 *
 * A: ???
 *
 * Q: Why does Resource::load() search for resource to load from higher to
 *    lower … Since it will load stuff until no more resources are marked as
 *    “Need to be loaded”.
 *
 * A: ???
 *
 *
 * Original DOS version
 * --------------------
 *
 * Banks: 1,236,519 B
 * exe:      20,293 B
 *
 * | Total bank              | size: 1236519 (100%) |
 * | :---------------------- | -------------------: |
 * | Total RT_SOUND          | size: 585052  ( 47%) |
 * | Total RT_MUSIC          | size:   3540  (  0%) |
 * | Total RT_POLY_ANIM      | size: 106676  (  9%) |
 * | Total RT_PALETTE        | size:  11032  (  1%) |
 * | Total RT_BYTECODE       | size: 135948  ( 11%) |
 * | Total RT_POLY_CINEMATIC | size: 291008  ( 24%) |
 *
 * As usual sounds are the most consuming assets (Quake1, Quake2, etc…)
 *
 *
 * memlist.bin features 146 entries
 * --------------------------------
 *
 * Most important part in an entry are:
 *
 * bankId:          - Give the file were the resource is.
 *
 * offset:          - How much to skip in the file before hiting the resource.
 *
 * size,packetSize: - How much to read, should we unpack what we read.
 *
 *
 * Polygons drawing
 * ----------------
 *
 * Polygons can be given as:
 *     - a pure screenspace sequence of points: Fabien Sanglard call those
 *       “screenspace polygons”.
 *     - a list of delta to add or substract to the first vertex.
 *       Fabien Sanglard call those “objectspace polygons”.
 *
 *
 * Video
 * -----
 *
 * Q: Why 4 framebuffer?
 *
 * A: It seems the background is generated once (like in the introduction) and
 *    stored in a framebuffer. Everyframe the saved background is copied and
 *    new elements are drawn on top.
 *
 *
 * Trivia
 * ------
 *
 * If you are used to RGBA 32bits per pixel framebuffer you are in for a
 * shock: Another world is 16 colors palette based, making it 4bits per pixel!
 *
 * Video generation
 * ----------------
 *
 * The engine sets the palette before starting to drawing instead of after
 * bliting. Fabien Sanglard used it to generate screenshots.
 *
 *
 * Memory management
 * -----------------
 *
 * There is 0 malloc during the game. All resources are loaded in one big
 * buffer (Resource::load). The entire buffer is “freed” at the end of a game
 * part.
 *
 * The renderer is actually capable of Blending a new poly in the framebuffer
 * (Video::drawLineT)
 *
 *
 * Fabien Sanglard is almost sure that:
 *
 *     _curPagePtr1 is the backbuffer
 *
 *     _curPagePtr2 is the frontbuffer
 *
 *     _curPagePtr3 is the background builder.
 *
 *
 * RETARTED
 * --------
 *
 * Why does memlist.bin uses a special state field 0xFF in order to mark the
 * end of resources? It would have been so much easier to write the number of
 * resources at the beginning of the code.
 *
 * \todo Use Boost.log for debug messages.
 *
 * \todo This code uses way to many pointers, this has to be fixed.
 *
 * \todo There is some concern with the timer to be fixed.
 *
 * \todo Add analysis from rawgl (<https://github.com/cyxx/rawgl>).
 *
 * \todo Add support of every version, using work from rawgl.
 *
 * \todo Add OpenGL support, using work from rawgl.
 *
 * \todo This code uses a system abstraction which serve no purpose and should
 * be removed.
 *
 * \todo Add support for gamepads and joysticks.
 *
 * \todo Use system thread to take advantage of actual pre-emptive
 * multi-threading.
 *
 * \todo Add screen-shot capability.
 *
 * \todo Add full screen option.
 *
 * \todo Add functionality to set sound volume.
 */

#include <boost/program_options.hpp>
#include <config.hpp>
#include <iostream>
#include <string>
#include <filesystem>

#include "engine.hpp"
#include "sys.h"
#include "util.h"

/**
 * \brief Main function of the program.
 * \param argc Count of arguments transmitted to the program.
 * \param argv Values of arguments transmitted to the program.
 * \return 0 if everything went well,
 *     -1 if the given data path does not exists,
 *     -2 if the given data path does not lead to a directory.
 */
int main(int argc, char **argv) {
    namespace po = boost::program_options;
    namespace fs = std::filesystem;

    /* -- Reading the command line. -- */

    /* Path to game data. */
    std::string dataPath;
    /* Path where to save game. */
    std::string savePath;

    /* Declaring supported options. */
    po::options_description desc("Supported options");
    desc.add_options()("help,h", "Display this help message.")(
        "version,v", "Display program version.")(
        "datapath,d", po::value<std::string>(&dataPath)->default_value("."),
        "Path to game data.");
    /* Command line. */
    po::options_description cmd;
    cmd.add(desc);

    /* Options map. */
    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).options(cmd).run(), vm);
    po::notify(vm);

    /* Indicates whether or not the program should be stopped. */
    bool stop = false;

    /* Check if help message should be displayed. */
    if (vm.count("help")) {
        std::cout << "Raw is a reimplementation of the virtual machine for "
                  << "Another World.\n\n"
                  << "Command: \n\n"
                  << '\t' << argv[0] << " [Options]\n\n"
                  << desc << '\n';
        stop = true;
    }

    /* Check if program version should be displayed. */
    if (vm.count("version")) {
        std::cout << argv[0] << " version " << Configuration::versionMajor
                  << '.' << Configuration::versionMinor << '.'
                  << Configuration::patchVersion << '\n';
        stop = true;
    }

    if (stop) return 0;

    if (!fs::exists(dataPath)) {
        std::cerr << "Data path incorrect: “" << dataPath
                  << "” does not exists.\n";
        return -1;
    }
    if (!fs::is_directory(dataPath)) {
        std::cerr << "Data path incorrect: “" << dataPath
                  << "” does not lead to a directory.\n";
        return -2;
    }

    /* -- Launch game. -- */

    /* System description. */
    extern System *stub;

    /* Game engine instantiation. */
    AnotherWorld::Engine e(stub, dataPath/*, savePath*/);

    return e.run();
}
