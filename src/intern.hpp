#ifndef __INTERN_HPP__
#define __INTERN_HPP__

/**
 * \file intern.hpp
 * \brief Useful definitions.
 * \author Montoir, Gregory
 * \author Sanglard, Fabien
 * \author Le Bars, Yoann
 * \version 2.0
 * \date 2021/04/27
 */

#include <cassert>
#include <cstdio>
#include <cstring>

#include "endian.h"
#include "util.h"

/// \brief Namespace for the game objects and functions.
namespace AnotherWorld {
    /// \brief Pointer to fetch byte or word from memory.
    struct Ptr {
        /// \brief A pointer to some memory space.
        uint8_t *pc;

        /**
         * \brief Fetch a byte in memory and jump to the next byte.
         * \return The value of the pointed byte.
         */
        uint8_t fetchByte () {return *pc++;}

        /**
         * \brief Fetch a word in memory and jump to the next word.
         * \return The value of the pointed word.
         */
        uint16_t fetchWord() {
            uint16_t i = READ_BE_UINT16(pc);
            pc += 2;
            return i;
        }
    };

    /// \brief A 2D point.
    class Point {
        public:
            /// \brief Default constructor.
            Point (): x_ (0), y_ (0) {}

            /**
             * \brief Constructs a Point using to coordinates.
             * \param _x Abscissa.
             * \param _y Ordinate.
             */
            Point (int16_t _x, int16_t _y): x_ (_x), y_ (_y) {}

            /**
             * \brief Access to abscissa, modification is not possible.
             * \return The abscissa.
             */
            int16_t x () const {return x_;}

            /**
             * \brief Access to abscissa, modification is possible.
             * \return A reference to the abscissa.
             */
            int16_t &x () {return x_;}

            /**
             * \brief Access to ordinate, modification is not possible.
             * \return The ordinate.
             */
            int16_t y () const {return y_;}

            /**
             * \brief Access to abscissa, modification is possible.
             * \return A reference to the ordinate.
             */
            int16_t &y () {return y_;}

        private:
            /// \brief Abscissa.
            int16_t x_;

            /// \brief Ordinate.
            int16_t y_;
    };
}

#endif
