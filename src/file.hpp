#ifndef __FILE_HPP__
#define __FILE_HPP__

/**
 * \file file.hpp
 * \brief Class to manage files into the game.
 * \author Montoir, Gregory
 * \author Sanglard, Fabien
 * \author Le Bars, Yoann
 * \version 2.0
 * \date 2021/04/26
 */

#include "intern.hpp"

struct File_impl;

namespace AnotherWorld {
    struct File {
        File_impl *_impl;

        File(bool gzipped = false);
        virtual ~File();

        bool open(const char *filename, const char *directory,
                  const char *mode = "rb");
        void close();
        bool ioErr() const;
        void seek(int32_t off);
        void read(void *ptr, uint32_t size);
        uint8_t readByte();
        uint16_t readUint16BE();
        uint32_t readUint32BE();
        void write(void *ptr, uint32_t size);
        void writeByte(uint8_t b);
        void writeUint16BE(uint16_t n);
        void writeUint32BE(uint32_t n);
    };
}

#endif
