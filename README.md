NEO-RAW
=======

This is an Another World VM implementation.  Based on Gregory Montoir’s
original work, the codebase has been cleaned up with legibility and
readability in mind. It has been tested on
[GNU](https://www.gnu.org/ "The GNU project")/[Linux](https://www.kernel.org/ "The Linux Kernel"),
[Microsoft Windows<sup>®</sup>](http://windows.microsoft.com/ "Microsoft Windows"),
[macOS<sup>®</sup>](https://www.apple.com/macos/ "macOS") and
[Rapsbian](https://www.raspbian.org/ "Raspbian").

If you never played to Another World, you can try it on-line:

<https://archive.org/details/msdos_Out_of_This_World_1991>

Architecture
------------

Fabien Sanglard published an extensive code analysis, available at the
following URLs:

<http://fabiensanglard.net/anotherWorld_code_review/index.php>

<http://fabiensanglard.net/another_world_polygons/index.html>

<http://fabiensanglard.net/another_world_polygons_PC_DOS/index.html>

Documentation specific to this code is available here:

<https://le-bars.net/yoann/AnotherWorld/>

About
-----

raw is a re-implementation of the engine used in the game Another World. This 
game, released under the name Out Of This World in non-European countries, was 
written by Eric Chahi at the beginning of the ’90s. More information can be 
found here:

http://www.mobygames.com/game/sheet/p,2/gameId,564/

Supported Versions
------------------

English PC DOS version is supported (“Out of this World”).

Compiling
---------

For compiling this code, you need a C++-2017 compliant compiler,
[SDL2](https://www.libsdl.org/ "SDL2"),
[ZLib](https://www.zlib.net/ "ZLib") and
[Boost](https://www.boost.org/ "Boost") (more specifically
Boost.program_options). To deal with dependencies, on
Microsoft Windows<sup>®</sup> you must use
[Conan](https://conan.io/ "Conan C/C++ Package Manager"). On
macOS<sup>®</sup> you can use either Conan,
[Hombrew](http://brew.sh/ "Homebrew"),
[MacPorts](https://www.macports.org/ "MacPorts"), or
[Fink](https://www.finkproject.org/ "Fink"). On GNU/Linux and
[*BSD](http://www.bsd.org/ "BSD"), you can use either the standard package
manager or Conan. On non Microsoft Windows<sup>®</sup> systems, to use Conan
set the `USE_CONAN` variable to `TRUE`.

This code has been tested on GNU/Linux.

Compiling is pretty straightforward. We recommend you create a directory
called `build` in the main directory. Then go to this directory and simply use
CMake to create compilation file for your environment. For instance, on POSIX™
compliant operating systems, the following commands will compile Raw (when you
are on `build` directory):

```
$ cmake ..
$ make
```

The resulting object file will then be located in
`build/src/bin/${COMPILATION_MODE}/`, where `${COMPILATION_MODE}` is the name
of the compilation mode used, such as “Debug” or “Release.” To install the
software, type as root:

```
# make install
```

To create documentation, simply use the “doc” entry in your compilation
files. For instance, on a POSIX™ compliant operating system:

```
$ make doc
```

Resulting documentations will be in `doc/`.

Of course, types of compilation can be changed using CMake, see CMake
documentation for more details.

To obtain some help, simply type:

```
$ raw --help
```

The documentation generates man pages.

If the compiler you are using is GCC or CLang on a x86_64 architecture, we
suggest you set the following variables (adapt this to your compiler and
architecture if needed):

```
//Flags used by the compiler during debug builds.
CMAKE_CXX_FLAGS_DEBUG:STRING=-g -pedantic -Wall -Werror -pipe

//Flags used by the compiler during release minsize builds.
CMAKE_CXX_FLAGS_MINSIZEREL:STRING=-march=native -Os -DNDEBUG -pipe

//Flags used by the compiler during release builds (/MD /Ob1 /Oi
// /Ot /Oy /Gs will produce slightly less optimized but smaller
// files).
CMAKE_CXX_FLAGS_RELEASE:STRING=-march=native -O3 -DNDEBUG -pipe

//Flags used by the compiler during Release with Debug Info builds.
CMAKE_CXX_FLAGS_RELWITHDEBINFO:STRING=-march=native -O2 -g -pipe -fno-omit-frame-pointer
```

Running
-------

You will need the original files, here is the required list :

* `BANK*`
* `MEMLIST.BIN`

To start the game, you can either:

* put datafiles of the game in the same directory as the executable;
* use the --datapath command line option to specify the datafiles directory.

Here are the various in game hotkeys:

* Arrow Keys: allow you to move Lester
* Enter/Space: allow you run/shoot with your gun
* C: allow to enter a code to jump at a specific level
* P: pause the game
* Alt X: exit the game
* TAB: change window scale factor

Credits
-------

[Éric Chahi](http://www.anotherworld.fr/ "Éric Chahi’s website"), obviously,
for making this great game.

Licence
-------

This software is distributed under GNU GPLv2 licence. It can be
copied and modified freely as long as initial authors are cited and the
license is kept the same. Complete text of the licence is in `LICENSE.txt`
file and can be found
[on-line](https://choosealicense.com/licenses/gpl-2.0/ "GNU GPLv2").

POSIX is a registered trademark owned by
[the Open Group](http://opengroup.org/ "The Open Group").

Linux is a registered trademark owned by
[Torvalds, Linus](https://github.com/torvalds "Torvalds, Linus on Github.").

macOS is a registered trademark owned by
[Apple](https://www.apple.com/ "Apple").

Microsoft Windows is a registered trademark owned by
[Microsoft](https://www.microsoft.com/ "Microsoft").

Copyright © 2004 – 2021.
