## Another World bytecode interpreter project definition.

cmake_minimum_required(VERSION 3.13)

# Name of the executable.
set(EXECUTABLE_NAME raw)

project(${EXECUTABLE_NAME} CXX)

set(CMAKE_CXX_STANDARD 17)

# Set version information.
set(VERSION_MAJOR 2)
set(VERSION_MINOR 1)
set(PATCH_VERSION 0)

add_definitions(-DBYPASS_PROTECTION -DAUTO_DETECT_PLATFORM)

# Whether or not use Conan to deal with dependencies.
#set (USE_CONAN FALSE)
# Conan is mandatory on Microsoft Windows®.
if(WIN32)
    set(USE_CONAN TRUE)
endif(WIN32)

if(USE_CONAN)
    include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
    conan_basic_setup(NO_OUTPUT_DIRS)
endif(USE_CONAN)

add_subdirectory(src)
add_subdirectory(doc)

install(
    TARGETS ${EXECUTABLE_NAME}
    CONFIGURATIONS Release
)

include(InstallRequiredSystemLibraries)
set(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_CURRENT_SOURCE_DIR}/LICENSE.txt")
set(CPACK_PACKAGE_VERSION_MAJOR "${VERSION_MAJOR}")
set(CPACK_PACKAGE_VERSION_MINOR "${VERSION_MINOR}")
include(CPack)
